import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
LED = 17
ledState = False

GPIO.setup(LED,GPIO.OUT)

while (True):
	ledState = not ledState
	GPIO.output(LED, ledState)
	ledState = not ledState
	GPIO.output(LED, ledState)
	ledState = not ledState
	GPIO.output(LED, ledState)
	ledState = not ledState
	GPIO.output(LED, ledState)
